# Java Course | #02 Java Programming - Calculator with source (BASIC)

[![Java Programming Lesson 3](http://img.youtube.com/vi/IgjKk7gpkb4/0.jpg)](https://youtu.be/IgjKk7gpkb4)

*__Kindly Like, Subscribe and Share the YouTube Video__*

## Java Course - Lesson 3

Welcome to Java Course (Lesson 3), in this session, we will develop a simple __Cryptocurrency Converter__. We can learn the
Java Array, For-each loop, Methods/Function, If-else, also the useful IntelliJ short-cut key. Wish you guys can become a
stronger Java developer/programmer.

*Feel free to comment and let us know what kind video you would like to watch*

## Exercise Answer

```
/**
 * To Convert USD to BTC
 *
 * @param usd number of usd
 * @return number of btc
 */
public static double usd2btc(double usd) {
    return usd / BTC_TO_USD_RATE;
}
```

```
// To test the usd2btc method
System.out.printf("Exercise Answer: %.6f USD is equal to %.6f BTC %n", totalNumberOfUSD, usd2btc(totalNumberOfUSD));
```