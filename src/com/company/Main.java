package com.company;

public class Main {

    // BTC -> USD Rate
    static final double BTC_TO_USD_RATE = 49063.50;

    public static void main(String[] args) {

        // Define the amount that you have in different wallets or platform
        double[] btcAmounts = new double[]{1, 3, 0.3, 0.004};

        // Define a variable to store total number of BTC that you have
        double totalNumberOfBTC = 0;

        // Using a loop to sum of all btc
        for (double btc : btcAmounts) {
            totalNumberOfBTC += btc; // syntax is equal to totalNumberOfBTC = totalNumberOfBTC + btc;
            System.out.printf("Sub Total: %.6f BTC, %.6f USD %n", btc, btc2usd(btc));
        }

        System.out.printf("Total number of BTC: %.6f %n", totalNumberOfBTC);

        //Covert BTC to USD
        double totalNumberOfUSD;
//        totalNumberOfUSD = totalNumberOfBTC * BTC_TO_USD_RATE;
        totalNumberOfUSD = btc2usd(totalNumberOfBTC);
        System.out.printf("Total number of USD: %.6f %n", totalNumberOfUSD);

        //Build a if-else logic here

        if (totalNumberOfUSD > 100000) {
            System.out.println("You have more than 100k USD!!!");
        } else {
            System.out.println("You have less than 100k USD....");
        }

        // Exercise Answer
        System.out.printf("Exercise Answer: %.6f USD is equal to %.6f BTC %n", totalNumberOfUSD, usd2btc(totalNumberOfUSD));

    }

    /**
     * To Convert BTC to USD
     *
     * @param btc number of btc
     * @return number of usd
     */
    // Generic conversion logic here
    public static double btc2usd(double btc) {
        return btc * BTC_TO_USD_RATE;
    }


    /**
     * To Convert USD to BTC
     *
     * @param usd number of usd
     * @return number of btc
     */
    // Exercise Answer
    public static double usd2btc(double usd) {
        return usd / BTC_TO_USD_RATE;
    }
}
